const inventory = require('./services/inventory');
const billing = require('./services/billing');
const marketing = require('./services/marketing');
const express = require('express');
const path = require('path');
const MongoClient = require('mongodb').MongoClient;
const app = express();
const collection_name = 'lab2';
const crypto = require('crypto');
const bodyParser = require('body-parser')
const util = require('util');
const rp = require('request-promise');
const KEY = crypto.randomBytes(32);
const IV = crypto.randomBytes(16);

const decrypt = (text,key,iv) => {
    let encryptedText = Buffer.from(text, 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key,'hex'), Buffer.from(iv, 'hex'));
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

const encrypt = (text,key,iv) => {
    const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return encrypted.toString('hex');
}



const getBooks = async (url) => {

    try {
        const db = await new MongoClient.connect(url);
        const collection = db.collection(collection_name);
        return collection.find().toArray();
    }
   catch(err){
        console.log(err);
    }
};

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// Put all API endpoints under '/api'
app.get('/api/passwords', (req, res) => {
    const count = 5;

    // Generate some passwords
    const passwords = Array.from(Array(count).keys()).map(i =>
        generatePassword(12, false)
    )

    // Return them as json
    res.json(passwords);

    console.log(`Sent ${count} passwords`);
});

app.get('/api/db', async (req,res) => {
   if (process.env.MONGODB_URI === undefined ||  process.env.MONGODB_URI=== null)
       res.status(404).send('DB not found');

   const books = await getBooks(process.env.MONGODB_URI);
   console.log('resturning',books);
   res.json(books);
   console.log(`Sent books`);

});

app.get('/api/info', async (req,res) => {
    if (process.env.APP_NAME === undefined ||  process.env.APP_NAME=== null)
        res.status(404).send('APP_NAME not found');

    const value = encrypt(process.env.APP_NAME,KEY,IV);
    const result= {
        key: KEY.toString('hex'),iv : IV.toString('hex'),value
    };
    console.log('returning ',result);
    res.json(result);

});

app.post('/api/decrypt', async (req,res) => {
    console.log('req.body',req.body);

    const {iv,key,value} = req.body;

   const result = decrypt(value,key, iv);
   console.log('result', result);
   res.json(result);

});


app.get('/api/marketing', async (req,res) => {
   console.log('in marketing');
   const results = await marketing.get;
   console.log('returning',results);
   res.json(results)
});
app.get('/api/billing', async (req,res) => {
    console.log('in billing');
    const results = await billing.get;
    console.log('returning',results);
    res.json(results)
});

app.get('/api/inventory', async (req,res) => {
    console.log('in inventory');
    const results = await inventory.get;
    console.log('returning',results);
    res.json(results)
});

app.get('/api/microservice/marketing', async (req,res) => {
    console.log('in microservice marketing');
    if(process.env.SERVICE_MARKETING === undefined || process.env.SERVICE_MARKETING === null) {
        console.error('SERVICE_INVENTORY not found');
        res.status(404).send('SERVICE_INVENTORY not found');
    }
    else{
        const results = await rp(process.env.SERVICE_MARKETING);
        console.log('returning',results);
        console.log('type of results: ', typeof(results));
        if(typeof(results) === 'string'){
            console.log('parsing to JSON');
            res.json(JSON.parse(results));
        }
        else {
            res.json(results);
        }
    }
});
app.get('/api/microservice/billing', async (req,res) => {
    console.log('in microservice billing');
    if(process.env.SERVICE_BILLING === undefined || process.env.SERVICE_BILLING === null) {
        console.error('SERVICE_INVENTORY not found');
        res.status(404).send('SERVICE_INVENTORY not found');
    }
    else{
        const results = await rp(process.env.SERVICE_BILLING);
        console.log('returning',results);
        console.log('type of results: ', typeof(results));
        if(typeof(results) === 'string'){
            console.log('parsing to JSON');
            res.json(JSON.parse(results));
        }
        else {
            res.json(results);
        }
    }
});

app.get('/api/microservice/inventory', async (req,res) => {
    console.log('in microservice inventory');
    if(process.env.SERVICE_INVENTORY === undefined || process.env.SERVICE_INVENTORY === null){
        console.error('SERVICE_INVENTORY not found');
        res.status(404).send('SERVICE_INVENTORY not found');
    }
    else{
        const results = await rp(process.env.SERVICE_INVENTORY);
        console.log('returning',results);
        console.log('type of results: ', typeof(results));
        if(typeof(results) === 'string'){
            console.log('parsing to JSON');
            res.json(JSON.parse(results));
        }
        else {
            res.json(results);
        }
    }
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log(`Webapp listening on ${port}`);

