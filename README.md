# LAB 3 - Monolith vs Microservices

Small ( as usual ) monolith web application that represent a big company. this application has 3 big bricks, **the billing 
brick, the inventory brick and the marketing brick**. Our objective is to show a webpage that shows the data ( from a mongodb 
database ) with all these bricks.

First you will do this as a Monolith, then you will have to level up the game by dividing each brick into a service. 

As always, good luck!

## Pre-Tasks
You will need a **Gitlab account** and **Heroku** with an **API_KEY**, if you dont have this, ask one of your teammates to help you ( lab1 & lab2 ).

 ## Tasks
 
 ### Let's connect the monolith ( 40% )
 As we did before, you will have to run this app in Heroku (name syntax: `EPF_ISM_LAB3_TX`), you should know how to do this, if not look to previous labs (remember to add the `APP_NAME` environment variable).
 
 - Once your app is running, you will see that you will have some "missing services" on the webapp. you job is to update the code
 under the `services` and/or `index.js` directory to make this work.
 - You will also need to connect a MongoDB database, you can use this link with the a working database `MONGODB_URI: mongodb://heroku_tch19335:i9lp8quth5m3p5sir0parr90eq@ds337718.mlab.com:37718/heroku_tch19335`
 - Once you have this working, you should have a list with information from all services. To make this simpler just send 
 me the link of your application and the name of your teammates through Slack ( on the channel `MSI`)
 - `excellence point*` add a timer that will show how much time it took the application to do the calls to each brick. 
 - `extra point` if you make your own DB ( you can use your pc or Heroku ) and add the collections as the code is looking 
  for send me also your `DATABASE_URL` and if it works you will get an extra point ( this is individually ) 
 
 ### Let's do this the microservice way ( 60% )
 
 Now you're an expert, but we have to make this look real. We need to separate the bricks from the same application. Your
 job is to use the code in this application to create others applications and connect them.
 
 - Create a Git project in your own git account for each brick ( Billing, inventory and marketing ) 
 - Create a Node app base on this project, that will only have 1 route ( `GET /api` ) that will return the same content as
 in this application
 - Create an Heroku application with the following syntax `EPF_ISM_LAB3_TX_BRICK_BRICKNAME`, remember to change the `BRICKNAME` 
 for th name of the corresponding brick
 - Add a `.gitlab-ci.yml` to your project ( you can copy/paste the one on this project and change the corresponding variables )
 - Once your apps are done you can send me each link ( one by brick )
 
 Now you have services separated that returns a value, you need to link them to the app:
 
 - in the Heroku of your monolith application, add an environment variable for each Service/brick with the following syntax: 
 KEY: `SERVICE_MARKETING` value: `URL` -> remember the URL is the link with the `/api` at the end. (`SERVICE_MARKETING` | 
 `SERVICE_INVENTORY` | `SERVICE_BILLING` )
 - Once this is done you can go to your monolith application on `/microservices` and you will see that it can connect to each service
 - Once this is done send me a message on slack so i can test it.
  - `excellence point*` add a timer that will show how much time it took the application to do the calls to each brick (the microservices bricks). 
 
 ### Full microservices (extra 2 points ~ 10%)
 So far we deployed microservices, but our `monolith` has the code of the backend and frontend are in the same project. 
 Show me watch you got and deploy this 2 services separately, if you have `CORS` problems send me a message and i will help
 you solve it. Once this is done just send me the link of your app running somewhere ( you're not obliged to use Heroku )


### What are excellence points?
As you might get, this means you're working for excellency, this will get you to 20, if you know what are you doing and 
you want to get the extra mille this is the way, but attention, dont focus on excellency points.

My advice: **50% of something its better than 100% of nothing**
Good luck!


### Help
The following commands will help you to create the application:
- `npm init` (to create the app)
- `npm install mongodb@^2.2.33 ` (this is the exact version you need to connect to the DB)
- `npm install express` (this is your webserver)
- `npm install` (to download all the dependencies)
- `npm start` (to run the app) 

The following files are important for Heroku:
- `Procfile` (just copy paste it)
- `package.json` (this is how it will run the app)

While coding the Microservice, remember you have to set the PORT as in this app (reference in `index.js`)

### Guides
- https://devcenter.heroku.com/articles/getting-started-with-nodejs
- https://expressjs.com/en/starter/hello-world.html

### Note
- If you're not sure how to create an application, i advice you to just copy this project and modify what you need, it will be easier for you.
- at the end of the class i will show 1 microservice, if you haven't developed at that moment your team will automatically 
loose `10%` of the task value (this meaning about 1~2/20 points) 
- You dont need to develop it using Node, any kind of web server will do the job.

<!-- if you find this say "I like reading REAL TEXT" to get an extra point -->
