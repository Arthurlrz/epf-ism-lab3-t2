const MongoClient = require('mongodb').MongoClient;
const collection_name = 'marketing';

const marketing_information = () => {
    return {status : 'OK'}
};
const get_marketing =async() =>{
    try {
        const db = await new MongoClient.connect(process.env.MONGODB_URI);
        const collection = db.collection(collection_name);
        return collection.find().toArray();
    }
    catch(err){
        console.log(err);
    }
};
module.exports = {
    information: marketing_information(),
    get: get_marketing()
};
