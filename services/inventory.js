const MongoClient = require('mongodb').MongoClient;
const collection_name = 'inventory';

const inventory_information = () => {
    return {status : 'OK'}
};
const get_inventory =async() =>{
    try {
        const db = await new MongoClient.connect(process.env.MONGODB_URI);
        const collection = db.collection(collection_name);
        return collection.find().toArray();
    }
    catch(err){
        console.log(err);
    }
};
module.exports = {
    information: inventory_information(),
    get: get_inventory()
};
