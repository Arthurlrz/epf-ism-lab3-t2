import React, { Component } from 'react';
import './App.css';

class MonolithApp extends Component {

    state = { books: [] ,appInformation: {}, marketingInformation: null, inventoryInformation: null, billingInformation: null};

    componentDidMount() {
        this.getBillingInformation();
        this.getInventoryInformation();
        this.getMarketingInformation();
        this.getAppInformation();
    }

    getMarketingInformation = () => {
        fetch('/api/marketing')
            .then(res => res.json())
            .then(marketingInformation => this.setState({marketingInformation}))
    };
    getBillingInformation = () => {
        fetch('/api/billing')
            .then(res => res.json())
            .then(billingInformation => this.setState({billingInformation}))
    };
    getInventoryInformation = () => {
        fetch('/api/inventory')
            .then(res => res.json())
            .then(inventoryInformation => this.setState({inventoryInformation}))
    };
    getAppInformation = () => {
        fetch('/api/info')
            .then(res => res.json())
            .then(appInformation => this.setState({ appInformation }));
    };

    renderAppInformation(appInformation){
        return(
          <div>
              <p>key: {appInformation.key}</p>
              <p>iv: {appInformation.iv}</p>
              <p>value: {appInformation.value}</p>

          </div>
        );
    }
    renderNoInformation(){
        return(
          <p>There is no app information</p>
        );
    }
    renderBill(bill,index){
        console.log('bill',bill)
        return(
            <div key={index}>
                <p>Client {bill.client} bought {bill.quantity} of {bill.item} for a price of {bill.price}</p>
            </div>
        );
    }
    renderBilling(){
        const {billingInformation} = this.state;
        console.log('billingInformation',billingInformation);
        return(
            billingInformation
            &&
            <ul className="books">
                {billingInformation.map(this.renderBill)}
            </ul>
            ||
            <p>Service is not available </p>
        );
    }
    renderItem(item,index){
        console.log('item',item);
        return(
            <div key={index}>
                <p >{item.name} - {item.quantity}</p>
                <img src={item.image_url} className="itemImage"/>
            </div>
        );
    }
    renderInventory(){
        const {inventoryInformation} = this.state;
        console.log('inventoryInformation',inventoryInformation);
        console.log('inventoryInformation type:',typeof(inventoryInformation));

            return(
                inventoryInformation
                &&
                <ul className="books">
                    {inventoryInformation.map(this.renderItem)}
                </ul>
                ||
                <p>Service is not available </p>
            ) ;
    }
    renderAd(ad,index){
        console.log('ad',ad)
        return(
            <div key={index}>
                <a href={ad.url}>{ad.title}</a>
            </div>
        );
    }
    renderMarketing(){
        const {marketingInformation} = this.state;
        console.log('marketingInformation',marketingInformation);
        return(
            marketingInformation
            &&
            <ul className="books">
                {marketingInformation.map(this.renderAd)}
            </ul>
            ||
            <p>Service is not available </p>
        );
    }
    render() {
        const { appInformation } = this.state;
        return (
            <div className="App">
                <h1>Monolith App information</h1>
                {appInformation && this.renderAppInformation(appInformation) || this.renderNoInformation()}
                <h1>Services</h1>
                <h2>Billing</h2>
                {this.renderBilling()}
                <h2>Inventory</h2>
                {this.renderInventory()}
                <h2>Marketing</h2>
                {this.renderMarketing()}
            </div>
        );
    }
}

export default MonolithApp;
