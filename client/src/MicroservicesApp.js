import React, {Component} from 'react';
import './App.css';

class MicroservicesApp extends Component {

    state = {
        books: [],
        appInformation: {},
        marketingInformation: null,
        inventoryInformation: null,
        billingInformation: null
    };

    componentDidMount() {
        this.getBillingInformation();
        this.getInventoryInformation();
        this.getMarketingInformation();
        this.getAppInformation();
    }

    getMarketingInformation = () => {
        fetch('/api/microservice/marketing')
            .then(res => res.json())
            .then(marketingInformation => this.setState({marketingInformation}))
    };
    getBillingInformation = () => {
        fetch('/api/microservice/billing')
            .then(res => res.json())
            .then(billingInformation => this.setState({billingInformation}))
    };
    getInventoryInformation = () => {
        fetch('/api/microservice/inventory')
            .then(res => res.json())
            .then(inventoryInformation => this.setState({inventoryInformation}))
    };
    getAppInformation = () => {
        fetch('/api/info')
            .then(res => res.json())
            .then(appInformation => this.setState({appInformation}));
    };

    renderAppInformation(appInformation) {
        return (
            <div>
                <p>key: {appInformation.key}</p>
                <p>iv: {appInformation.iv}</p>
                <p>value: {appInformation.value}</p>

            </div>
        );
    }

    renderNoInformation() {
        return (
            <p>There is no app information</p>
        );
    }

    renderBill(bill, index) {
        console.log('bill', bill)
        return (
            <div key={index}>
                <p>Client {bill.client} bought {bill.quantity} of {bill.item} for a price of {bill.price}</p>
            </div>
        );
    }

    renderBilling() {
        let {billingInformation} = this.state;
        console.log('billingInformation', billingInformation);
        console.log('billingInformation type:', typeof (billingInformation));
        if (typeof (billingInformation) === "string")
            billingInformation = JSON.parse(billingInformation);
        if (billingInformation) {
            if (Array.isArray(billingInformation)) {
                return (
                    <ul className="books">
                        {billingInformation.map(this.renderBill)}
                    </ul>);
            } else {
                return (<p>invalid object</p>);
            }
        } else {
            return (<p>Service is not available </p>);
        }
    }

    renderItem(item, index) {
        console.log('item', item);
        return (
            <div key={index}>
                <p>{item.name} - {item.quantity}</p>
                <img src={item.image_url} className="itemImage"/>
            </div>
        );
    }

    renderInventory() {
        let {inventoryInformation} = this.state;
        console.log('inventoryInformation', inventoryInformation);
        console.log('inventoryInformation type:', typeof (inventoryInformation));
        if (typeof (inventoryInformation) === "string")
            inventoryInformation = JSON.parse(inventoryInformation);
        if (inventoryInformation) {
            if (Array.isArray(inventoryInformation)) {
                return (
                    <ul className="books">
                        {inventoryInformation.map(this.renderItem)}
                    </ul>
                );
            } else {
                return (<p>invalid object</p>);
            }
        } else {
            return (<p>Service is not available </p>);
        }

    }

    renderAd(ad, index) {
        console.log('ad', ad)
        return (
            <div key={index}>
                <a href={ad.url}>{ad.title}</a>
            </div>
        );
    }

    renderMarketing() {
        let {marketingInformation} = this.state;
        console.log('marketingInformation', marketingInformation);
        console.log('marketingInformation type:', typeof (marketingInformation));
        if (typeof (marketingInformation) === "string")
            marketingInformation = JSON.parse(marketingInformation);
        if (marketingInformation) {
            if (Array.isArray(marketingInformation)) {
                return (
                    <ul className="books">
                        {marketingInformation.map(this.renderAd)}
                    </ul>);
            } else {
                return (<p>invalid object</p>);
            }
        } else {
            return (<p>Service is not available </p>);
        }
    }

    render() {
        const {appInformation} = this.state;
        return (
            <div className="App">
                <h1>Microservice App information</h1>
                {appInformation && this.renderAppInformation(appInformation) || this.renderNoInformation()}
                <h1>Services</h1>
                <h2>Billing</h2>
                {this.renderBilling()}
                <h2>Inventory</h2>
                {this.renderInventory()}
                <h2>Marketing</h2>
                {this.renderMarketing()}
            </div>
        );
    }
}

export default MicroservicesApp;
