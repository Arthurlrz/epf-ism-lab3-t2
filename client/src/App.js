import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import MicroservicesApp from "./MicroservicesApp";
import MonolithApp from "./MonolithApp";

export default function BasicRouter() {
    return (
        <Router>
            <div>
                <ul>
                    <li>
                        <Link to="/monolith">Monolith</Link>
                    </li>
                    <li>
                        <Link to="/microservices">Microservices</Link>
                    </li>
                </ul>

                <hr />

                {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
                <Switch>
                    <Route exact path="/monolith">
                        <MonolithApp />
                    </Route>
                    <Route path="/microservices">
                        <MicroservicesApp />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}
